#ifndef _TODO_H
#define _TODO_H

#include <linux/types.h>
#include <linux/list.h>  // struct list_head, list_for_each, list_entry, list_add_tail, list_del


/*
*******************************************************************************

                            Data Structures For TODO list

*******************************************************************************
*/

struct todo_list_t
{
    struct list_head list;
    int status;
    char *data;
    int size;
    time_t deadline;
};

/*Dov:
*******************************************************************************

                                Public functions for TODO list

*******************************************************************************
*/
void del_todo_list_from_task_struct(struct task_struct* task);



/*
*******************************************************************************

                                TODO sys calls

*******************************************************************************
*/
ssize_t sys_add_TODO(pid_t pid, const char *TODO_description, ssize_t description_size, time_t deadline);


ssize_t sys_read_TODO(pid_t pid, int TODO_index, char*TODO_description, time_t* deadline, int* status);


ssize_t  sys_mark_TODO(pid_t pid, int TODO_index, int status);


ssize_t  sys_delete_TODO(pid_t pid, int TODO_index);


ssize_t punish(void);
#endif // _TODO_H
