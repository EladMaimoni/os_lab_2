#!/bin/bash
cd /usr/src/linux-2.4.18-14custom
cp /mnt/hgfs/Shared/sources/* ./ -R
make bzImage
if [ "$1" = "full" ]; then
	echo full build - makeing modules
	make modules
	echo full build - installing modules
	make modules_install
	echo full build - modules installed
fi
make install
cd /boot
mkinitrd -f 2.4.18-14custom.img 2.4.18-14custom
echo build complete
