#include <linux/todo.h>
#include <asm/uaccess.h>
#include <linux/sched.h> // for_each_task, find_task_by_pid, CURRENT_TIME
#include <linux/errno.h> 
#include <linux/slab.h>  // kmalloc, kfree

/*
*******************************************************************************

                            Helper Functions

*******************************************************************************
*/

/*
 is_descendant:
  check if pid is a descendant of current process
 
 @return:
   1 (true)  - pid is a descendant of current process
   0 (false) - pid is not a descendant of current process
*/
static int is_descendant(pid_t pid) 
{
    // obtain the process task struct
    struct task_struct* target_process = find_task_by_pid(pid);
    
    // go up the process tree verify that we don't
    // meet the current process.
    while (NULL != target_process)
    {
        if (target_process->pid == current->pid)
        {
            return 1;
        }
        if (0 == target_process->pid)
        {
           return 0;
        }
        target_process = target_process->p_pptr;         
    }
    
    return 0;

}

/*
 get_todo_from_task:
  
 
 @return:
   a pointer to the to the todo_list_t struct according to given index
   if not found, NULL is returned.

*/
static struct todo_list_t* get_todo_from_task(struct task_struct *task, int index) 
{
    if (index < 0) return NULL;
    
    struct todo_list_t* returned_todo = NULL;
    
    // get a handle to the list:
    struct list_head *list_handle = &(task->todos_list.list);
    
    {
        // iterate through list:
        int i = 1;
        struct list_head *current_list_element = NULL; // current list element
        
        list_for_each(current_list_element, list_handle) 
        {   
            if (i++ == index)
            {
                // obtain the todo field from the current_list_element:
                returned_todo = list_entry(
                    current_list_element,
                    struct todo_list_t /* field type */ ,
                    list /* field name (member) */
                    );
                    
                break;
            }
        } 
    }
    return returned_todo;
}

/*
 get_task_from_pid:
  
 
 @return:
   a pointer to the task_struct with given pid.

*/
static struct task_struct* get_task_from_pid(pid_t pid) {
    struct task_struct *task = NULL;
    for_each_task(task) 
    {
        if (task->pid == pid)
        {
            return task;
        }            
    }
    return NULL;
}

/*
 get_task_from_pid:
   removes and delete a todo from the todo list

*/
static void del_todo_from_list(struct todo_list_t* todo) 
{
    list_del(&(todo->list));
    kfree(todo->data);
    kfree(todo);
}

/*Dov:
   del_todo_list_from_task_struct:
    removes the whole todo_list of a task struct.
    This function should be called when a proccess is terminated
    and the memory of its todo_list needs to be freed.
*/
void del_todo_list_from_task_struct(struct task_struct* task)
{
    struct list_head *list_handle = &(task->todos_list.list);
    struct list_head *current_list_element = NULL;
    struct todo_list_t* returned_todo = NULL;
    
    list_for_each(current_list_element, list_handle)
    {   
        returned_todo = list_entry(
            current_list_element,
            struct todo_list_t /* field type */ ,
            list /* field name (member) */
            );
        del_todo_from_list(returned_todo);
    } 
}

/*
 current_has_late_todo:
  check if the current process has a late task and remove it
 
 @return:
   1 - true  - if late todo was found and removed
   0 - false - no late todos

*/
static int current_has_late_todo(void) 
{
    struct list_head *p = NULL;
    struct todo_list_t *todo = NULL;
    time_t now = CURRENT_TIME;
    
    list_for_each(p, &(current->todos_list.list)) 
    {
        todo = list_entry(p, struct todo_list_t, list);
        
        if ((todo->deadline < now) && (todo->status == 0)) 
        {
            // we found a TODO who's task hasn't completed yet
            // and its deadline has passed
            
            //Dov: Delete the task from the list
            del_todo_from_list(todo);
            return 1;
        }
    }
    return 0;
}


/*
*******************************************************************************

                            system calls implementation

*******************************************************************************
*/


/*
 sys_add_TODO:
  Add a TODO to the TODO�s queue of a process identified by pid
 
 @return:
 // 0 1 or special values???
   0 - sucess
   -1 - failure
*/

ssize_t sys_add_TODO(pid_t pid, const char *TODO_description, ssize_t description_size, time_t deadline)
{
	// check arguments
    if ((NULL == TODO_description) || (description_size < 1) || (deadline < CURRENT_TIME))
	{
        return -EINVAL;
    }
	
	struct task_struct* task_struct_ptr = NULL;
    struct todo_list_t* new_todo_list_ptr   = NULL;

    
    // A process can add a TODO only to itself or to its descendants.
	// so we need to make sure pid is either of the current process or of one of its
	// descendants:
    if (!is_descendant(pid))
	{
		return -ESRCH;
	}
        
    task_struct_ptr = get_task_from_pid(pid);
    
	if (NULL == task_struct_ptr)
	{
        return -ESRCH;
	}
    
    // allocate todo, fill todo fields
    new_todo_list_ptr = (struct todo_list_t*)kmalloc(sizeof(struct todo_list_t), GFP_KERNEL);
    if (NULL == new_todo_list_ptr)
	{
        return -ENOMEM;
	}
	
    new_todo_list_ptr->status = 0; // uncompleted
    new_todo_list_ptr->size = description_size;
    new_todo_list_ptr->deadline = deadline;
    new_todo_list_ptr->data = (char*)kmalloc(description_size, GFP_KERNEL);
    if (NULL == new_todo_list_ptr->data) 
	{
        kfree(new_todo_list_ptr);
        return -ENOMEM;
    }
	
    if (copy_from_user(new_todo_list_ptr->data, TODO_description, description_size)) 
	{
        kfree(new_todo_list_ptr->data);
        kfree(new_todo_list_ptr);
        return -EFAULT;
    }

    // instead of just appending todo to todos list, we take into account its deadline: 
    struct list_head *p = NULL;
    struct todo_list_t *next_todo = NULL;
    
    list_for_each(p, &(task_struct_ptr->todos_list.list)) 
    {
        next_todo = list_entry(p, struct todo_list_t, list);
        // if next todo has a larger deadline, break and insert:
        if (next_todo->deadline > new_todo_list_ptr->deadline) break;
    }
    list_add_tail(&(new_todo_list_ptr->list), p);
    
    return 0;
}


/*
 sys_read_TODO:
   Get the description of TODO identified by its position.
 
 @return:
   > 0: number of chars copied
   < 0: error code
*/
ssize_t sys_read_TODO(pid_t pid, int TODO_index, char *TODO_description, time_t* deadline, int *status)
{
    struct task_struct* task_struct_ptr = NULL;
    struct todo_list_t* todo_list_ptr = NULL;

    // check arguments
    if ((NULL == TODO_description))
	{
        return -EINVAL;
    }
    
    // check permissions
    if (!is_descendant(pid))
	{
        return -ESRCH;
    }
	task_struct_ptr = get_task_from_pid(pid);
    if (NULL == task_struct_ptr)
	{
        return -ESRCH;
    }
	
    // get todo by index
    todo_list_ptr = get_todo_from_task(task_struct_ptr, TODO_index);
    if (NULL == todo_list_ptr) 
    {
        return -EINVAL;
    }

    // copy to user
    if (copy_to_user(TODO_description, todo_list_ptr->data, todo_list_ptr->size)) 
	{
        // copy failure
        return -EFAULT;
    }
	
    if (copy_to_user((void*)status, (void*)(&todo_list_ptr->status), sizeof(todo_list_ptr->status))) 
	{
        // copy failure
        return -EFAULT;
    }
    
    if (copy_to_user((void*)deadline, (void*)(&todo_list_ptr->deadline), sizeof(todo_list_ptr->deadline))) 
	{
        // copy failure
        return -EFAULT;
    }
    
    return todo_list_ptr->size;
}


/*
 sys_mark_TODO:
   Set the status of a TODO identified by its position.
 
 @return:
 // 0 1 or special values???
   0 - sucess
   1 - failure
*/
ssize_t sys_mark_TODO(pid_t pid, int TODO_index, int status)
{
    struct task_struct* task_struct_ptr = NULL;
    struct todo_list_t* todo_list_ptr   = NULL;

    
    // A process can change the status of a TODO that belongs to itself or to
	// one of its descendants.
	// so we need to make sure pid is either of the current process or of one of its
	// descendants:
    if (!is_descendant(pid))
	{
		return -ESRCH;
	}
        
    task_struct_ptr = get_task_from_pid(pid);
    if (NULL == task_struct_ptr)
	{
        return -ESRCH;
	}
	
    todo_list_ptr = get_todo_from_task(task_struct_ptr, TODO_index);
    if (NULL == todo_list_ptr)
	{
        return -EINVAL;
	}
    //Dov: Allow changing the status only if the dealdline hasn't passed yet
    if (todo_list_ptr->deadline >= CURRENT_TIME){
        todo_list_ptr->status = status;
	}
    return 0;
}

/*
 sys_delete_TODO:
   Delete a TODO identified by its position.
 
 @return:
 // 0 1 or special values???
   0 - sucess
   -1 - failure
*/
ssize_t sys_delete_TODO(pid_t pid, int TODO_index)
{
    struct task_struct* task_struct_ptr = NULL;
    struct todo_list_t* todo_list_ptr = NULL;


    // check permissions:
    if (!is_descendant(pid))
    {
       return -ESRCH; 
    }
        
    task_struct_ptr = get_task_from_pid(pid);
    if (NULL == task_struct_ptr)
    {
       return -ESRCH; 
    }
        
    todo_list_ptr = get_todo_from_task(task_struct_ptr, TODO_index);
    
    if (NULL == todo_list_ptr)
    {
        return -EINVAL;
    }
        
    
    del_todo_from_list(todo_list_ptr);
    return 0;
}

ssize_t punish(void)
{
    // runs on every return from system call.
    // don't handle swapper (pid=0)
    if (current->pid == 0) return 0;
    if (current_has_late_todo()) 
    {
        set_current_state(TASK_INTERRUPTIBLE);
        schedule_timeout(60 * HZ);
    }
    return 0;
}
