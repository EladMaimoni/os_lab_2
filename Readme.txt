General notes:

Explanation about the working environment:
	On the Red hat virtual machine I created a shared folder at /mnt/hgfs/Shared which is linked to my
	repository on the host. I suggest you set it up the same way.

Explanation about files in the project:

	sources/
		This is a folder that contains the kernel source and header files that we will need to modify.
		They are contained in the same folder structure as in the acutal kernel so that they can easily
		be copied to there each time before the kernel is compiled. 

	build_kernel.sh
		This file is a bash script that copies the files from the "sources" folder into their place in the kernel,
		thus modifying the files in the actual kernel. Then the script performs all the stages explained in the exercise pdf
		to compile the kernel except for the reboot command.
		if build_kernel.sh is run with the argument 'full' (./build_kernel.sh full) stages 4-5 in the compilation process
		("make modules" and "make modules-install") are performed, otherwise - they are skipped.
