#include <errno.h>
#include <sys/types.h>


/*
*******************************************************************************

                            System Calls Wrappers - TODO

*******************************************************************************
*/


int add_TODO(pid_t pid, const char *TODO_description, ssize_t description_size, time_t deadline) 
{
	int res;
	__asm__
		(
		"pushl %%eax;"
		"pushl %%ebx;"
		"pushl %%ecx;"
		"pushl %%edx;"
        "pushl %%esi;"
		"movl $243, %%eax;"  //sys call num
		"movl %1, %%ebx;"    //arg 1
		"movl %2, %%ecx;"	 //arg 2
		"movl %3, %%edx;"	 //arg 3
        "movl %4, %%esi;"	 //arg 4
		"int $0x80;"
		"movl %%eax,%0;"
        "popl %%esi;"
		"popl %%edx;"
		"popl %%ecx;"
		"popl %%ebx;"
		"popl %%eax;"
		: "=m" (res)
		: "m" (pid), "m" (TODO_description), "m"(description_size), "m" (deadline)
		);
	if (res >= (unsigned long)(-125))
	{
		errno = -res;
		res = -1;
	}
	return (int)res;
}


ssize_t read_TODO(pid_t pid, int TODO_index, char *TODO_description, time_t* deadline, int* status) {
	int res;
	__asm__
		(
		"pushl %%eax;"
		"pushl %%ebx;"
		"pushl %%ecx;"
		"pushl %%edx;"
		"pushl %%esi;"
		"pushl %%edi;"
		"movl $244, %%eax;"  //sys call num
		"movl %1, %%ebx;"    //arg 1
		"movl %2, %%ecx;"	 //arg 2
		"movl %3, %%edx;"	 //arg 3
		"movl %4, %%esi;"	 //arg 4
		"movl %5, %%edi;"	 //arg 5
		"int $0x80;"
		"movl %%eax,%0;"
		"popl %%edi;"
		"popl %%esi;"
		"popl %%edx;"
		"popl %%ecx;"
		"popl %%ebx;"
		"popl %%eax;"
		: "=m" (res)
		: "m" (pid), "m" (TODO_index), "m"(TODO_description), "m" (deadline), "m"(status)
		);
	if (res >= (unsigned long)(-125))
	{
		errno = -res;
		res = -1;
	}
	return (int)res;
}


int mark_TODO(pid_t pid, int TODO_index, int status) 
{
	int res;
	__asm__
		(
		"pushl %%eax;"
		"pushl %%ebx;"
		"pushl %%ecx;"
		"pushl %%edx;"
		"movl $245, %%eax;"  //sys call num
		"movl %1, %%ebx;"    //arg 1
		"movl %2, %%ecx;"	 //arg 2
		"movl %3, %%edx;"	 //arg 3
		"int $0x80;"
		"movl %%eax,%0;"
		"popl %%edx;"
		"popl %%ecx;"
		"popl %%ebx;"
		"popl %%eax;"
		: "=m" (res)
		: "m" (pid), "m" (TODO_index), "m"(status)
		);
	if (res >= (unsigned long)(-125))
	{
		errno = -res;
		res = -1;
	}
	return (int)res;
}


int delete_TODO(pid_t pid, int TODO_index) 
{
	int res;
	__asm__
		(
		"pushl %%eax;"
		"pushl %%ebx;"
		"pushl %%ecx;"
		"movl $246, %%eax;"  //sys call num
		"movl %1, %%ebx;"    //arg 1
		"movl %2, %%ecx;"	 //arg 2
		"int $0x80;"
		"movl %%eax,%0;"
		"popl %%ecx;"
		"popl %%ebx;"
		"popl %%eax;"
		: "=m" (res)
		: "m" (pid), "m" (TODO_index)
		);
	if (res >= (unsigned long)(-125))
	{
		errno = -res;
		res = -1;
	}
	return (int)res;
}
